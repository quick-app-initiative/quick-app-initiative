# QAI Launch Participant: **RSI**

<!-- LOGO HERE -->
[![RSI logo](xxxx)](target url)

RSI is a A new foundation in the process of being launched out of Switzerland to promote sustainability. RSI is interested in leveraging Quick Apps to enable sustainability in domains such as apparel and footwear.

<!-- additional text here -->


<!-- why be part of QAI, or do you lead a Task Force or Project, etc. -->


<!-- Quick App showcase (if pertinent): no more than 2 apps -->
<!-- Write a short introduction to your 1 or 2 apps. Explain why you showcase them -->
<!-- For each app provide image, name, where people find it,  link to YouTube video if available + 1 sentence to explain why it is interesting -->
<!-- Above all, BE REASONABLE, this is not a marketing website ! -->

<!-- Standardized organisation information -->
#
Country of incorporation/registration: Switzerland (registration pending)

Type of organisation: non-profit <!-- choose from: public service, non-profit, SME, large enterprise, multi-national, other (specify) -->

Domain(s) of activity: sustainability <!-- short list/keywords here -->

Website: <!-- website URL; normally the same as the logo click target -->

QAI representative: xxx

<!-- any other contact information that you would like to show -->
