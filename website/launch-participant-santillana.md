# QAI Launch Participant: **Santillana**

<!-- LOGO HERE -->
[![Santillana logo](https://santillana.com/en/Theme/SANTILLANA/img/logo-santillana.svg)](https://santillana.com/en/)

Santillana is a leading company that transformed education in Latin America and Spain through innovative platforms and digital content. Interested in exploring the Quick App paradigm as a new concept to deliver content to students.

<!-- additional text here -->


<!-- why be part of QAI, or do you lead a Task Force or Project, etc. -->


<!-- Quick App showcase (if pertinent): no more than 2 apps -->
<!-- Write a short introduction to your 1 or 2 apps. Explain why you showcase them -->
<!-- For each app provide image, name, where people find it,  link to YouTube video if available + 1 sentence to explain why it is interesting -->
<!-- Above all, BE REASONABLE, this is not a marketing website ! -->

<!-- Standardized organisation information -->
#
Country of incorporation/registration: Madrid, Spain

Type of organisation: Company <!-- choose from: public service, non-profit, SME, large enterprise, multi-national, other (specify) -->

Domain(s) of activity: Education <!-- short list/keywords here -->

Website: https://santillana.com/en/ <!-- website URL; normally the same as the logo click target -->

QAI representative: Alejandro Montarroso Estebanez

<!-- any other contact information that you would like to show -->
