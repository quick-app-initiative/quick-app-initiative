# QAI Launch Participant: **FRVR**

<!-- LOGO HERE -->
[![FRVR logo](https://corp.frvr.com/app/uploads/2020/12/logo.png)](https://frvr.com/)

FRVR is a successful games company actively using Quick Apps as one of the dozens of channels that its technology platform enables developers to publish to.

<!-- additional text here -->
FRVR enables game developers to bring their games to billions of users, and digital ecosystem owners to bring the perfect games directly to their users. With the average user's device computing power and internet connection speed skyrocketing, there is no technical reason why high-quality, gameplay-deep experiences can't be brought to users instantly, so long as the structural support is there.

FRVR is proud to head up the Games Task Force of the Quick App Initiative, helping to grow the market and spread understanding of this dramatic change in the way that quality games content can be brought directly to users.

<!-- why be part of QAI, or do you lead a Task Force or Project, etc. -->


<!-- Quick App showcase (if pertinent): no more than 2 apps -->
<!-- Write a short introduction to your 1 or 2 apps. Explain why you showcase them -->
<!-- For each app provide image, name, where people find it,  link to YouTube video if available + 1 sentence to explain why it is interesting -->
<!-- Above all, BE REASONABLE, this is not a marketing website ! -->

<!-- Standardized organization information -->
#
Country of incorporation/registration: Malta

Type of organization: SME <!-- choose from: public service, non-profit, SME, large enterprise, multi-national, other (specify) -->

Domain(s) of activity: Games Publishing, Games Development, Games Platform <!-- short list/keywords here -->

Website: https://frvr.com/ <!-- website URL; normally the same as the logo click target -->

QAI representatives:
* Chris Benjaminsen
* Kester Bishop

<!-- any other contact information that you would like to show -->
