# OW2 Quick App initiative (QAI)

This initiative is focused on nurturing a _Quick App_ ecosystem in Europe and beyond through multi-party, meritocratic and transparent collaboration.

Quick App is a new platform for light hybrid applications that do not require installation. **For users**, it enables an "_in the moment_" app dynamic, whilst delivering a near native app experience. **For developers**, it requires less coding, and provides the possibility to distribute outside of traditional app stores. **For business**, it provides a powerful dynamic for generating new user experiences and capturing new custsomers.

Liberating for users - democratising for developers - empowering for business.

- **Watch** the [launch video](https://www.ow2con.org/view/2021/).
- **Visit** the [portal](https://quick-app-initiative.ow2.io/).
- **subscribe** to the [public mailing list](https://mail.ow2.org/wws/info/quickapp).

... And of course, we would love to welcome you on board!
[Join here](https://www.ow2.org/view/QuickApp/Participants_Form).


## Digging deeper

Quick App is a way to develop, package, and distribute MiniApp compliant applications across platforms, facilitating the process of development through advanced UI components and predefined native APIs (e.g., push notifications, network monitoring, Bluetooth, camera, homescreen icon ...) that enable developers to design and build usable, reliable and efficient applications rapidly. What's more, as the MiniApp standard matures, the implementation of runtime engines for devices facilitates a "_code once, deploy anywhere_" paradigm. A clear benefit for developers and businesses.

- Find out more in our [White Paper](https://quick-app-initiative.ow2.io/page/whitepaper/) or [Primer](https://quick-app-initiative.ow2.io/docs/Quick_App_Primer.pdf).

## QAI aims

The [OW2 Quick App initiative](https://quick-app-initiative.ow2.io/) promotes a **concrete implementation** of the abstract [MiniApp standard](https://github.com/w3c/miniapp/tree/gh-pages/specs), allowing light applications in native environments for smart devices.

The initiative explores vertical applications where Quick Apps have the potential to be part of a solution, as well as transversal activities that further core technologies across verticals, including (as appropriate) usability, accessibility, sustainability, privacy, security, and ethical standards.

The initiative has four key aims:
1. Raise awareness and understanding about Quick Apps in business and developer communities.
2. Develop a commons of technical knowledge, guidelines, and best practices.
3. Feed into the emerging W3C MiniApp standard (and act as a dissemination channel).
4. Develop Open Source code about Quick Apps. For example, tools, code templates, plugins for IDEs, test-beds, Etc.

- Read more in our [Charter](https://quick-app-initiative.ow2.io/docs/charter.pdf).

## QAI launch participants

QAI was launched during in June 2021 by the following organisations:
* [Huawei Europe](launch-participant-huawei.md) **(Initiative chair)**
* [CTIC](launch-participant-ctic.md)
* [Santillana](launch-participant-santillana.md)
* [FRVR](launch-participant-frvr.md)
* [Famobi](launch-participant-famobi.md)
* [European Alliance for Technology Development (Alliance Tech)](launch-participant-alliancetech.md)
* [RSI](launch-participant-rsi.md) (in process of incorporation)

## QAI open to all
* Any organization or individual regardless of geographic location;
* Operating system and device vendors that implement Quick App engines, marketplaces, and other supporting tools for Quick Apps;
* Content and service providers interested in end-user interactions using Quick Apps;
* Marketing experts interested in the promotion of Quick Apps as a paradigm;
* Developers, including professionals, hobbyists, and students, interested in Web and native app technologies;
* Public institutions, including municipalities, with specific needs such as accessible services for citizens and visitors;
* Research centers and academic institutions interested in innovation through agile technologies;
* Innovative entrepreneurs and SMEs.
* ...

To become a participant of the initiative please **[register](https://www.ow2.org/view/QuickApp/Participants_Form)** now.

## Let's stay in touch

- **Subscribe** to the [mailing list](https://mail.ow2.org/wws/info/quickapp).
- **Visit** the [portal](https://quick-app-initiative.ow2.io/).
- **Access** the [code repository](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative).
