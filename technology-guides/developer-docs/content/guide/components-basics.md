# Components Basics

## Base Example

Components are the modular elements to create quick app pages. They can be created with [essential elements](../components) like in HTML websites (`text`, `div`, `web`...) and reusing other custom components. 

Here's an example of a quick app component (stored as `./clickable-text.ux`):

``` html
<template> 
  <div class="container"> 
    <text onclick='onClick'>{{text}}</text> 
  </div> 
</template>

<style>
  .container {
    flex-direction: column;
    justify-content: center;
  }
</style>

<script>
  export default {
    private: { 
      text: 'my text'
    },       
    onInit() {
      this.text = 'Changed text'  
    },
    onClick() {
      console.log('Element clicked')
    } 
  }
</script>
```

You can create components using essential [quick app elements](../components) and other custom components.

## Reusing Custom Components

You can reuse components directly from other components in a hierarchical structure. 

You only need to import the custom component using the `<import>` element in the [UX document](./ux-documents).

For instance:

``` html {1,5}
<import name='clickable-text' src='./clickable-text'></import> 
<template>
  <div class="container">
    <text class="title">Hi Quick Apps!</text>
    <clickable-text></clickable-text>
  </div>
</template>
```
The `name` attribute indicates the name of the reused component, and `src` specifies the location of the UX file that describes the component. The filename extension `.ux` can be omitted in `src`.

If no `name` is set, the system will use the filename specified in `src` (without `.ux` extension) as the element name by default.

::: tip 
- The element name is case insensitive. By default, the element name is in lowercase.
- The `(on|@)event` syntax can be used to bind events of custom components. 
- It is recommended to use the hyphen character (`-`) to link the event to the method of the parent component (e.g., use `event-type1` instead of `eventType1`).
:::

## Content Distribution with Slots

Like in HTML elements, we can pass content to a component through the `slot` element. 

For instance, the custom component `cool-component` is defined as follows:

``` html {4}
<template> 
  <div> 
    <text>HEADER</text> 
    <slot>default content</slot> 
    <text>FOOTER</text> 
  <div> 
</template>
```

To use the custom component on another component, you can import it and include the text for the `slot`.

``` html {1,4}
<import name="cool-component" src="./cool-component"></import> 
<template> 
  <cool-component> 
    <text>This is the body passed thru</text> 
  </cool-component> 
</template>
```

The system renders the page with the following elements in the DOM:

``` html
<div> 
  <text>HEADER</text> 
  <text>This is the body passed thru</text> 
  <text>FOOTER</text> 
</div>
```

## Loading Dynamic Components

Dynamic components are a flexible way to determine the custom components to be rendered in the template in runtime.

To load components dynamically in other component you can use the `<component>` element, indicating the name of the component to be rendered in the expression of the attribute `is`.

``` html
<import name="component-1" src="./comp1"></import> 
<import name="component-2" src="./comp2"></import> 
<import name="component-3" src="./comp3"></import>  
<template> 
  <div> 
    <text>{{componentName}}</text> 
    <component is="{{who}}"></component> 
    <input type="button" onclick="onchangeComponent" value="changeComponent" /> 
  </div> 
</template>
<script> 
  export default { 
    data: { 
      componentName: 'Component dynamic component', 
      who: 'component-1' 
    }, 
    onchangeComponent() { 
      if (this.who === 'component-1') { 
        this.who = 'component-2'
        this.componentName: 'Component 2'
      } else if (this.who === 'component-2') { 
        this.who = 'component-3'
        this.componentName: 'Component 3'
      } else { 
        this.who = 'component-1' 
        this.componentName: 'Component 1'        
      } 
    } 
  } 
</script>
```
