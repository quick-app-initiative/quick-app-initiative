# Distribution

You can pack and distribute the `.apk` with your quick app using any method. Quick apps are also listed in specific marketplaces. 

The way to upload and list your quick app on a marketplace depends on the quick app vendor (for instance, you can see how to upload and create a [quick app in the AppGallery](https://developer.huawei.com/consumer/en/doc/development/quickApp-Guides/quickapp-create-quickapp-0000001079835824)).

Let us know if you have another marketplace to be listed here. 



