# Services Running in Background

In general, the resources used by a quick app are released once it is hidden in the background. However, you can also implement some features to keep specific services running in the background (e.g., music keeps playing). 

There are two ways for sending a quick app to the background:

- The user taps the device's __home button__ to access the home screen. 
- The user taps the __back button__, exiting the quick app. 

In the second case, the quick app continues running in the background only if the following conditions are met:

1.	The [manifest](./manifest) declares the required APIs to run in the background.
2.	The current page is the entry page of the quick app.
3.	At least one background running API (declared in the [manifest](./manifest)) is running.

::: tip
- Running an app in the background consumes a large number of system resources. Be sure to use only the necessary resources for your app.
- It is recommended that you place the APIs to run in the background in the [`app.ux`](./ux-documents.html#app-ux) document to avoid the impact of page switching and destruction. For instance, you can declare the [audio module](../services/audio) on all the pages that play music and use the `onshow` handler to implement the management of audio playback, pause, etc.
:::

## Supported APIs

Only the following services are allowed to run in the background:

- [`system.audio`](../services/audio): Audio player. 
- [`system.record`](../services/recording): Audio recording.
- [`system.request`](../services/request): Uploading/downloading data.
- [`system.geolocation`](../services/location): Get the location.


## Declaration of APIs

You need to declare the APIs that may run in the background of your app. Apart from indicating the [`features`](./manifest.html#features) in the [manifest](./manifest), you have to include these APIs in the [`config.background`](./manifest.html#background) member.

Example:

```json
{
  "config": {
    "background": {
      "features":[
        "system.request",
        "system.geolocation"
      ]
     }
  }
}
```
