# `web`

Container to render HTML pages. 

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/web01.gif" alt="Webview on a Quick App" /> 

([Example code](#example))

The web element supports file download, which can be implemented using the [Web Pages API](../services/webview).

[[toc]]

## Children Elements

This element doesn't support any element as child node.

## Attributes

This element may contain the following attributes.

- [`src`](#src)
- [`supportzoom`](#supportzoom)
- [`trustedurl`](#trustedurl)
- [`allowthirdpartycookies`](#allowthirdpartycookies)
- [`showloadingdialog`](#showloadingdialog)
- [`useragent`](#useragent)
- [`algorithmsforlayout`](#algorithmsforlayout)
- [`wideviewport`](#wideviewport)
- [`overviewmodeinload`](#overviewmodeinload)
- [`multiwindow`](#multiwindow)
- [`jumppolicy`](#jumppolicy)
- [`openinbrowserurl`](#openinbrowserurl)
- [`xrequestedwithpolicy`](#xrequestedwithpolicy)
- [`fullscreendirection`](#fullscreendirection)
- [`initialscale`](#initialscale)
- [`renamejsinterface`](#renamejsinterface)

### `src`

URL of a web resource to be loaded within the element.

- Type: `string` 
- Default value: - 
- Mandatory: no 

### `supportzoom`

Flag that indicates if the web resource can change its size (zoom in/out).

- Type: `boolean` 
- Default value: `false` 
- Mandatory: no 

### `trustedurl`

List of trusted URLs (whitelist), which can be specified as regular expressions. 

- Type: `array` 
- Default value: (the current value of `src`) 
- Mandatory: no 

::: warning
Only the websites specified in `trustedurl` or `src` support two-way communication.
:::

### `allowthirdpartycookies`

Flag that indicates if third-party cookies can be delivered across domains. 

- Type: `boolean` 
- Default value: `false` 
- Mandatory: no 

### `showloadingdialog`

 Flag to show or hide the progress bar.

- Type: `boolean` 
- Default value: `true` 
- Mandatory: no 

### `useragent`

User agent to be specified in the HTTP headers.

- Type: `string` 
- Default value: the user agent of the quick app.
- Mandatory: no 

::: tip User agent of the quick app
The user agent of the quick app follows this pattern: 
`hap/<Platform version number>/<Vendor ID> <Platform app package name>/<Platform app version number> <App package name>/<App version number> (<Source>)`. 
:::

### `algorithmsforlayout`

Attribute to specify the layout mode to adjust the WebView.

- Type: `string` (`normal` | `onecolumn` | `narrowcolumn` | `textautosize`)
  - `normal`: Rendering is not changed. Recommended for compatibility between different platforms and system versions.
  - `onecolumn`: All content is displayed in one column. The view width is the width of the column.
  - `narrowcolumn`: All columns are not wider than the screen.
  - `textautosize`: The font size of a paragraph is adjusted based on the heuristic method so that the text can be read when you view the wide viewport layout in overview mode. You are advised to enable zooming when using this mode.
- Default value: `narrowcolumn` 
- Mandatory: no 

### `wideviewport`

Flag to indicate if the WebView supports HTML `meta` to control the viewport or the wide viewport.

- Type: `boolean` 
  - `false`: The layout width is always set to the WebView control width in CSS pixels.
  - `true`: If the web page contains the viewport meta element, the system will use the width specified. If the page does not contain a tag or does not provide a width, the wide viewport is used.
- Default value: `true` 
- Mandatory: no 

### `overviewmodeinload`

Flag to indicate if a WebView loads pages in overview mode (i.e., zooming out on the content to make the content width shorter and fit into the screen). This attribute is used when the content width is greater than the width of the WebView, for example, when the wide viewport is enabled.

- Type: `boolean` 
- Default value: `false` 
- Mandatory: no 

### `multiwindow`

Indicates whether the web element supports multi-window display.

- Type: `boolean` 
- Default value: `false` 
- Mandatory: no 

### `jumppolicy`

Indicates the multi-window opening policy.

- Type: `string` (`browser` | `default`) 
  - `browser`: Multiple windows started in the web element are opened in the system browser.
  - `default`: Multiple windows started in the web element are opened in a quick app.
- Default value: `default` 
- Mandatory: no 

### `openinbrowserurl`

Attribute with an array of URLs that will be opened in the system browser. You can indicate them using regular expressions.

- Type: `array` 
- Default value: - 
- Mandatory: no 

### `xrequestedwithpolicy`

This attribute sets the value of the `x-requested-with` header in the HTTP/HTTPS request.

- Type: `number` 
  - `1`: The system uses the implementation by default. The system uses the engine package name by default (i.e., `<x-requested-with: packagename>`).
  - `0`: The system uses an empty header field value (i.e, `<x-requested-with: >`).
- Default value: `1` 
- Mandatory: no 

### `fullscreendirection`

Orientation of the web element in full-screen mode.

- Type: `string` (`landscape` | `portrait`)
- Default value: `landscape` 
- Mandatory: no 

### `initialscale`

Initial percentage of the content displayed in the web element (in `%`). If you set this attribute, `wideviewport` must be `false`.

- Type: `number` 
- Default value: - 
- Mandatory: no 

### `renamejsinterface`

Name of the API for the web element to communicate with the HTML page. 

- Type: `string`
- Default value: `system`
- Mandatory: no 

# Events

This element supports the [common events](./events), with the exception of [`swipe`](events.html#swipe). In addition to this, this element supports the following events:

- [`pagestart`](#pagestart)
- [`pagefinish`](#change)
- [`titlereceive`](#titlereceive)
- [`error`](#error)
- [`message`](#message)
- [`progress`](#progress)

### `pagestart` 

This event is triggered when a web page starts the loading process. 

__Additional parameters__: 
- `{ url: uri }`

### `pagefinish` 

This event is triggered when a web page is successfully loaded. 

__Additional parameters__: 
- `{ url: uri }`
- `{ canBack: boolean }`
- `{ canForward: boolean }`

### `titlereceive` 

This event is triggered when the a web page title is received.

__Additional parameters__: 
- `{ title: string }`

### `error` 

This event is triggered when the web page opening or loading fail. 

__Additional parameters__: 
- `{ errorMsg: string }`

### `message` 

This event is triggered when a message is received from the web page. 

__Additional parameters__: 
- `{ message: string }`
- `{ url: uri }`

### `progress` 

This event is triggered when the progress of the web page loading changes. 

__Additional parameters__: 
- `{ newProgress: string }` (range `0` to `100`).

## Methods

This element has the following methods:

- [`reload()`](#reload)
- [`forward()`](#forward)
- [`back()`](#back)
- [`canForward({callback})`](#canforward-callback)
- [`canBack({callback})`](#canback-callback)
- [`postMessage({message})`](#postmessage-message)

### `reload()`

Force the reload of a page.

__Parameters__:
- `object` with the following attributes:
  - `index: number` (mandatory). Index with the position to display

### `forward()`

Load of the next page in the browse history.

### `back()`

Load of the previous page in the browse history.

### `canForward({callback})`

Method to determine if the forward browse option is supported.

__Parameters__:
- `callback: function(res)`.

The function receives `res`, a `boolean` that indicates if the option is supported (`true`) or not.


### `canBack({callback})`

Method to determine if the backward browse option is supported.

__Parameters__:
- `object` with the following attributes:
  - `callback: function(res)`. 

The function receives `res`, a `boolean` that indicates if the option is supported (`true`) or not.


### `postMessage({message})`

Sends messages to a web page.

__Parameters__:
- `object` with the following attributes:
  - `message: string`

## Example

Main page component:

``` html
<template>
  <div class="container row-center column-center">
    <text class="btn-transparent" onclick="qaiClick">Quick App Initiative</text>
  </div>
</template>

<script>
  import router from '@system.router';
  module.exports = {
    qaiClick() {
      router.push({
        uri: '/Component/basic/web/detail',
        params: { webUrl: "https://quick-app-initiative.ow2.io/" }
      })
    }
  }
</script>
```

Details page, implemented in the component with `/Component/basic/web/detail` path:

``` html
<template>
      <div class="container">
        <web src="{{webUrl}}" useragent="{{ua}}" ontitlereceive="titleReceived"></web>
    </div>
</template>

<script>
  module.exports = {
    public: {
        webUrl:"",
        ua:"",
    },
    titleReceived({title}){
      this.$page.setTitleBar({text: title});
    }
  }
</script>
```