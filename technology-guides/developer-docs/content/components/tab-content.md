# `tab-content`

Content corresponding to a tab within a [`tabs`](./tabs) element.

This element will have the same height as the [`tabs`](./tabs) parent element, excluding the [`tab-bar`](./tab-bar). The children of this element will be arranged horizontally in the container.

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/tabs01.jpg" alt="Tabs on a Quick App" /> 

([Example code](#example))

[[toc]]

## Children Elements

All children [elements](./) are supported.

## Attributes

In addition to the [common attributes](./attributes), this element may contain the following attribute.

- [`scrollable`](#scrollable)

### `scrollable`

This flag indicates whether a user can swipe the page to switch among tab pages. 

- Type: `boolean` 
- Default value: `true` 
- Mandatory: no 

If you set this attribute to `false`, you have to implement the switch mechanism among the [`tab-bar` elements](./tab-bar).


## CSS Properties

This element supports the [common styles](./styles).

## Events

This element support all the [common events](./events), with the exception of [`swipe`](events.html#swipe).

## Methods

This element does not have additional methods.

## Example

``` html
<template> 
    <div class="container"> 
        <tabs class="tabs" onchange="changeTabactive" index="2"> 
            <tab-bar class="tab-bar"> 
                <text class="tab-text">tab1</text> 
                <text class="tab-text">tab2</text> 
                <text class="tab-text">tab3</text> 
            </tab-bar> 
            <tab-content class="tab-content" scrollable="{{scrollable}}"> 
                <div class="item-container"> 
                    <div class="item-content"> 
                        <text class="item-title">tab1 page</text> 
                    </div> 
                </div> 
                <div class="item-container"> 
                    <div class="item-content"> 
                        <text class="item-title">tab2 page</text> 
                    </div> 
                </div> 
                <div class="item-container"> 
                    <div class="item-content"> 
                        <text class="item-title">tab3 page</text> 
                    </div> 
                </div> 
            </tab-content> 
        </tabs> 
    </div> 
</template> 


<style lang="sass">
   .tabs { 
        flex: 1; 
        margin-top: 20px; 
        margin-right: 30px; 
        margin-left: 30px; 
        background-color: #ffffff; 
    } 
    .tab-content { 
        flex: 1; 
    } 
    .tab-bar { 
        height: 100px; 
        border-color: #bbbbbb; 
        color: #bbbbbb; 
        border-bottom-width: 1px; 
    } 
    .tab-text { 
        width: 300px; 
        text-align: center; 
    } 
    .tab-text:active { 
        color: #f76160;
    } 
    .item-container { 
        padding-top: 30px; 
        padding-left: 30px; 
        padding-right: 30px; 
        flex-direction: column; 
    } 
    .item-content { 
        flex-direction: column; 
        padding-bottom: 30px; 
    } 
    .item-title { 
        padding-top: 50px; 
        padding-bottom: 20px; 
        color: #aaaaaa; 
    } 
</style> 
<script> 
    export default { 
        data: { 
            scrollable: true, 
        }, 
        changeTabactive: function (e) { 
            // switch tab 
            console.log('----------switch tab: ' + e.index); 
        }, 
    } 
</script>
```