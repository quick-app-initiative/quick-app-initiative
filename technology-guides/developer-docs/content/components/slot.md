# `slot`

Represents a placeholder for distributing content in child components. This element is similar to the [`slot` HTML templating element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Slot).

[[toc]]

## Children Elements

`slot` elements may have any children elements.

## Attributes

This element may contain the following attribute:

- [`name`](#name)

### `attribute`

Name of a slot. 

- Type: `string` 
- Default value: `default` 
- Mandatory: no 

If the element has the name attribute set up, it is called a _named slot_.

## Usage

You can use the `slot` element to define a placeholder inside a component and fill it in with content from other component. 

We can create the template with specific markup, and include a placeholder with `slot`.  

``` html
<!-- detail.ux template -->
<template> 
  <div> 
    <slot></slot> 
  </div> 
</template>
```

We can include the previous `detail.ux` template as a child of another component `main.ux` and distribute the content. 

``` html
<!-- main.ux template -->
<import name="detail" src="./detail.ux"></import> 
<template> 
  <div>
    <detail> 
      <text>Parent's content 1</text> 
    </detail> 
  </div> 
</template>
```

The two DOM trees will be presented together during the rendering process.

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/slot01.jpg" alt="Slot example in a Quick App" /> 


### Content by Default

You can include content by default in the `slot` element. This will be shown in case no value is defined by the parent.

``` html
<!-- detail.ux template -->
<template> 
  <div> 
    <slot> 
      <text>Default content in the child component</text> 
    </slot> 
  </div> 
</template>
```

``` html
<!-- main.ux template -->
<import name="detail" src="./detail.ux"></import> 
<template> 
  <div>
    <detail> 
    </detail> 
  </div> 
</template>
```

With the following result:

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/slot02.jpg" alt="Slot example in a Quick App" /> 

When the content is not distributed to the children templates, the value by default will be rendered with the content by default.

The following example renders the value by default.


``` html
<!-- main.ux template -->
<import name="detail" src="./detail.ux"></import> 
<template> 
  <div>
    <detail> 
        <text if="{{showContent}}">Dynamic content that may be shown...</text>
    </detail> 
  </div> 
</template> 
<script> 
export default { 
  data() { 
    return { 
      showContent: false 
    } 
  } 
} 
</script>
```

With this result: 

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/slot02.jpg" alt="Slot example in a Quick App" /> 

### Named Slot

In a component, `slot` elements with different names to refer to them from the external. If name is not set, `default` is used.

``` html
<!-- detail.ux template -->
<template> 
  <div> 
    <div class="header"> 
      <!-- Put the header here. --> 
      <slot name="header"></slot> 
    </div> 
    <div class="main"> 
      <!-- Put the body here. --> 
      <slot name="main"></slot> 
    </div> 
    <div class="footer"> 
      <!-- Put the footer here. --> 
      <slot name="footer"></slot> 
    </div> 
  </div> 
</template>
```

In the parent element, set the slot attribute. If it is not set, default is used. Then, the content is distributed to the corresponding slot element.

``` html
<!-- main.ux template -->
<import name="detail" src="./detail.ux"></import> 
<template> 
  <div>
    <detail> 
      <text slot="header">header content</text> 
      <text slot="main">main content</text> 
      <text slot="footer">footer content</text> 
    </detail> 
  </div> 
</template>
```
