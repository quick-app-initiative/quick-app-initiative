# Common Attributes

All the [essential elements](./) implements the following common attributes. Each element extends the list with other attributes depending on the case. Check the specific [element](./) to see more details on the concrete extensions.  

[[toc]]

## Basic Attributes

### `id`	

__Unique identifier of the element.__

- Type: `string`
- Value by default: -

### `style`

__Definition of the CSS styles inline.__

- Type: `string`
- Value by default: -

### `class`	

__Class to apply CSS styles on elements.__

- Type: `string`
- Value by default: -

### `disabled`	

__Indicates whether the current element is available for use (`false`) or not (`true`).__

- Type: `boolean` 
- Value by default: `false`

### `focusable`	

__Flag to indicate if the element can get the focus (`true`) or nor (`false`).__

- Type: `boolean` 
- Value by default: `false`

### `data`

__Auxiliary attribute to facilitate data storage and manipulation.__

- Type: `string`
- Value by default: -


### `dir` 

__Attribute that indicates the display writing direction of the element.__

- Type: `string` with the following values.
- `ltr`: from left to right
- `rtl`: from right to left
- `auto`: automatically set by the system

If this parameter is not set, this element inherits the value of parent element's `dir`. 
If the element is at the root of the `template` it will take the system settings by default.

::: warning
- The value `ltr` of this attribute does not take effect for the [`marquee` element](./marquee) when the script writing direction is `right-to-left` (e.g., in Arabic).
:::

## Rendering Logic Attributes

The [component template](../guide/templates) could include directives to [render blocks conditionally](../guide/templates.html#conditional-rendering). If the condition defined by the directive is not met, the element is removed from the Virtual DOM.

The following attributes set the directives to control the [conditional rendering](../guide/templates.html#conditional-rendering):

### `for`

__Loop on an array of elements.__

- Type: `array`	

### `if/elif/else`

__Boolean conditions to display or skip elements in the rendering process.__

- Type: `boolean`

### `show`	

__Boolean condition to display the current element (`true`) or not (`false`).__ This directive has the same behavior as `{ display: flex | none }`.

- Type: `boolean`

For details about how to use rendering attributes, check the [conditional rendering section](../guide/templates.html#conditional-rendering).

