# `canvas`

A canvas to draw graphics.

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/canvas01.gif" alt="Canvas on a Quick App" /> 

([Example code](#example))


[[toc]]

## Children Elements

This element doesn't support children elements.

## Attributes

In addition to the [common attributes](./attributes), this element may contain the following attributes.

- [`id`](#id)
- [`disable-scroll`](#disable-scroll)

### `id`

Attribute that indicates the identifier of the element. This attribute is mandatory.

- Type: `string` 
- Default value: - 
- Mandatory: yes

### `disable-scroll`

Attribute that indicates if the gesture events are bound to the canvas, users are allowed to scroll on the page or pull down to refresh data (when interacting with the canvas).

- Type: `boolean` 
- Default value: `false` 
- Mandatory: no


## CSS Properties

This element supports the [common styles](./styles).

## Events

In addition to the [common events](./events), this element supports the following events:

- [`longtap`](#longtap)
- [`error`](#error)

### `longtap` 

This event is triggered when a user touches the screen for `500 ms`. Once this event is triggered, scrolling is prevented.

### `error` 

This event is triggered when an error happens.

__Additional parameters__: 
- `{ errMsg: string }`. Reason of the error.

## Example

``` html
<template>
  <div class="container">
    <div class="mlr-container mt-item">
      <canvas id="canvas" class="canvas bro-l"></canvas>
    </div>
    <div class="mt-btn row-center">
      <text class="btn-transparent" onclick="drawPath">Draw close path</text>
    </div>
    <div class="btn-transparent row-center">
      <text class="btn-transparent" onclick="drawFont">Draw font</text>
    </div>
  </div>
</template>

<style lang="sass">
  .canvas {
    height: 400px;
    background-color: #fff;
  }
</style>

<script>
  module.exports = {
    drawPath() {
      let canvas = this.$element('canvas');
      let ctx = canvas.getContext('2d');
      ctx.clearRect(0,0,3000,3000);
      ctx.lineWidth = 5;
      ctx.beginPath();
      ctx.moveTo(250, 50)
      ctx.lineTo(250, 300)
      ctx.lineTo(450, 300)
      ctx.closePath()
      ctx.stroke();
    },
    drawFont() {
      let canvas = this.$element('canvas');
      let ctx = canvas.getContext('2d');
      ctx.clearRect(0,0,3000,3000);
      ctx.font = '50px';
      ctx.fillText("Hello World",200,200);
    }
  }
</script>
```