# Battery Level

__Device's battery level.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.battery"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import battery from '@system.battery' 
```

Or

``` js
let battery = require("@system.battery")
```


## Methods

This service has the following method:

- [`getStatus({success,fail,complete})`](#getstatus-success-fail-complete)

### `getStatus({success,fail,complete})`

__This method gets the battery level of the current device__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success, with the following arguments:
  - `res.charging` (`boolean`): flag that indicates if the battery is being charged.
  - `res.level` (`number`): Current battery level (values from `0.0` to `1.0`).
- `fail` (`function(code)`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
battery.getStatus({ 
    success(res) {
        console.log(`level: ${res.level}`);
    } 
})
```