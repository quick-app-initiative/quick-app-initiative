# Background Running

__Run the app in the background.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.resident"}
```

::: tip Usage note
Also, you need to indicate the services that may be active in the background by specifying the services in the `config.background` member of the manifest. Read all the details in the section [Services Running in the Background](../guide/background.html).
:::


## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import resident from '@system.resident' 
```

Or

``` js
let resident = require("@system.resident")
```

## Methods

This service has the following methods:

- [`start()`](#start)
- [`stop()`](#stop)

### `start()`

__This method sends an app to the background.__

Example:

``` js
resident.start({ 
    desc: ' ' 
})
```

### `stop()`

__Stops an app from running in the background.__


Example:

``` js
resident.stop()
```