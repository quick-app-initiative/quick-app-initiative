# Network Status

__Status of the current network connection__ (WiFi, 4G, 5G, ethernet,...).

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.network"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import network from '@system.network' 
```

Or

``` js
let network = require("@system.network")
```


## Methods

This service has the following methods:

- [`getType({success,fail,complete})`](#gettype-success-fail-complete)
- [`subscribe({callback,fail})`](#subscribe-callback-fail)
- [`unsubscribe()`](#unsubscribe)
- [`getSimOperator({success,fail,complete})`](#getsimoperator-success-fail-complete)


### `getType({success,fail,complete})`

__Method to get the type of the network connection__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `metered` (`boolean`). Flag to indicate if the network operator charges in function of the traffic.
  - `type` (`string`). Indicates the type of the network connection. Options are: `2g`, `3g`, `4g`, `5g`, `wifi`, `ethernet`, `bluetooth`, `other` and `none`.
- `fail` (`function`). Optional callback function corresponding to a failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.


Example:

``` js 
network.getType({ 
    success:function(data){console.log("handling success: " + data.type);} 
})
```

### `subscribe({callback,fail})`

__Listens to the network connection status__.

If you call this method multiple times, only the last call takes effect.

#### Arguments

This method requires an `object` with the following attributes:
- `callback`(`function(res)`). Optional callback function executed when the network information changes. The argument of the callback function is an `object` with the following members:
  - `metered` (`boolean`). Flag to indicate if the network operator charges in function of the traffic.
  - `type` (`string`). Indicates the type of the network connection. Options are: `2g`, `3g`, `4g`, `5g`, `wifi`, `ethernet`, `bluetooth`, `other` and `none`.
- `fail` (`function`). Optional callback function corresponding to a failed execution. 


Example:

``` js 
network.subscribe({ 
    callback:function(data){console.log("handling callback");} 
})
```

### `unsubscribe()`

__Cancels the listener on the network connection status__.

Example:

``` js 
network.unsubscribe()
```

### `getSimOperator({success,fail,complete})`

__Method to get the carrier information of a SIM card__.

::: warning
This method require specific permissions (access to phone calls).
:::

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `operators` (`array`). List of information for each SIM card in the device. Each item in the list is an `object` with the following members:
    - `operator` (`string`): Data network carrier information (mobile country code + mobile network code, or [MCC-MNC](https://www.mcc-mnc.com/)).
    - `slotIndex` (`number`): SIM slot number.
    - `isDefaultDataOperator` (`boolean`): Flag that indicates if the SIM is the default one for data connection in the system.
  - `size` (`number`). Number of SIM cards in the device.
- `fail` (`function(msg, code)`). Optional callback function corresponding to a failed execution. The result codes can be:
  - `201`: Failed to get the call permission. The user rejects authorization.
  - `1001`: No SIM card is inserted.
  - `1002`: Failed to get the carrier information. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.


Example:

``` js 
network.getSimOperators({ 
    success(data) { 
        console.log(`size: ${data.size}`) 
        for (const i in data.operators) { 
            console.log(`operator: ${data.operators[i].operator}, 
                    slotIndex:${data.operators[i].slotIndex}, 
                    isDefaultDataOperator:${data.operators[i].isDefaultDataOperator},` 
            ) 
        } 
    }, 
    fail(erromsg, errocode) { 
        console.log('errocode: ' + errocode + ' – erromsg: ' + erromsg) 
    } 
})
```

### `getNetworkOperator({success,fail,complete})`

__Method to get the carrier of the network connected by a SIM card__.

::: warning
This method require specific permissions (access to phone calls).
:::

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `operators` (`array`). List of information for each SIM card in the device. Each item in the list is an `object` with the following members:
    - `operator` (`string`): Data network carrier information (mobile country code + mobile network code, or [MCC-MNC](https://www.mcc-mnc.com/)).
    - `slotIndex` (`number`): SIM slot number.
    - `isDefaultDataOperator` (`boolean`): Flag that indicates if the SIM is the default one for data connection in the system.
  - `size` (`number`). Number of SIM cards in the device.
- `fail` (`function(msg, code)`). Optional callback function corresponding to a failed execution. The result codes can be:
  - `201`: Failed to get the call permission. The user rejects authorization.
  - `1001`: No SIM card is inserted.
  - `1002`: Failed to get the carrier information. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.


Example:

``` js 
network.getNetworkOperator({ 
    success(data) { 
        console.log(`size: ${data.size}`) 
        for (const i in data.operators) { 
            console.log(`operator: ${data.operators[i].operator}, 
                    slotIndex:${data.operators[i].slotIndex}, 
                    isDefaultDataOperator:${data.operators[i].isDefaultDataOperator},` 
            ) 
        } 
    }, 
    fail(erromsg, errocode) { 
        console.log('errocode: ' + errocode + ' – erromsg: ' + erromsg) 
    } 
})
```
