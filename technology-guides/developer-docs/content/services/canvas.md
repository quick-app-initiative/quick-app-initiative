# Canvas

__Manipulation of canvas elements for drawings.__

## Declaration

You don't need to declare the use of this service in the [manifest`](../guide/manifest.html) or import it from your scripts. Canvas is part of the built-in elements implemented by the quick app platform.

You can access a drawing context using the `getContext()` (only the `2d` dimension is supported) method on the canvas element, method that returns a rendering `object` with the following methods and attributes. 

Example:

``` js
const canvasComponent = this.$element(canvasId) 
const canvas = canvasComponent.getContext("2d")
```


## Attributes

A rendering `object` has the following attributes:

- `fillStyle` (`<color>|<gradient><pattern>`). Sets the fill style.
- `strokeStyle` (`<color>|<gradient><pattern>`). Sets the stroke style.
- `lineWidth` (`number`). A number specifying the line width, in pixels.
- `lineCap` (`number`). Line end style. The options are `butt` (default), `round`, and `square`.
- `lineJoin` (`string`). Style of the intersection point of lines. The options are `bevel`, `round`, and `miter` (default).
- `miterLimit` (`number`). Maximum miter length.
- `font-style` (`string`). Font style. The options are `normal`, `italic`, and `oblique`.
- `font-weight` (`string`). Font weight. The options are `normal` and `bold`.
- `font-family` (`string`). Font family. The options are `sans-serif`, `serif`, and `monospace`.
- `font-size` (`string`). Font size, in pixels.
- `textAlign` (`string`). Text alignment mode. The options are `left`, `center`, `right`, `start` (default), and `end`.
- `textBaseline` (`string`). Text alignment mode in the horizontal direction. The options are `top`, `bottom`, `middle`, `hanging`, `alphabetic` (default), and `ideographic`.
- `globalAlpha` (`number`). Global alpha value of a canvas (from `0` or transparent to `1`, opaque).
- `globalCompositeOperation` (`string`). Indicates how to draw the overlapped part when an image is placed on another. The options are `source-over` (default), `source-atop`, `source-in`, `source-out`, `destination-over`, `destination-atop`, `destination-in`, `destination-out`, `lighter`, `copy`, and `xor`. If this attribute is set, PDF canvas drawing with anti-aliasing is not supported.
- `lineDashOffset` (`number`). Dash line offset. The default value is `0`.
- `shadowOffsetX` (`number`). Horizontal shadow offset against the drawn object. The value by default is `0`.
- `shadowOffsetY` (`number`). Vertical shadow offset against the drawn object. The default value is `0`.
- `shadowBlur` (`number`). Level of shadow blur. The default value `0` represents no blur and larger numbers represent increasingly more blur.
- `shadowColor` (`string`). Shadow color. The default value is `black`.


## Methods

Any element support the following method:

- [`toTempFilePath({options})`](#totempfilepath-options)
- [`createLinearGradient()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/createLinearGradient/createLinearGradient)
- [`addColorStop()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/addColorStop)
- [`createRadialGradient()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/createRadialGradient)
- [`rect()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/rect)
- [`fillRect()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/fillRect)
- [`strokeRect()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/strokeRect)
- [`clearRect()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/clearRect)
- [`fill()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/fill)
- [`stroke()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/stroke)
- [`beginPath()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/beginPath)
- [`closePath()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/closePath)
- [`moveTo()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/moveTo)
- [`lineTo()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/lineTo)
- [`arc()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/arc)
- [`bezierCurveTo()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/bezierCurveTo)
- [`quadraticCurveTo()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/quadraticCurveTo)
- [`scale()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/scale)
- [`rotate()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/rotate)
- [`translate()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/translate)
- [`clip()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/clip)
- [`fillText()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/fillText)
- [`drawImage()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage)
- [`save()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/save)
- [`restore()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/restore)
- [`createPattern()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/createPattern)
- [`arcTo()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/arcTo)
- [`transform()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/transform)
- [`setTransform()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/setTransform)
- [`measureText()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/measureText)
- [`createImageData()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/createImageData)
- [`getImageData()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/getImageData)
- [`putImageData()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/putImageData)
- [`strokeText()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/strokeText)
- [`setLineDash()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/setLineDash)
- [`getLineDash()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/getLineDash)


### `toTempFilePath({options})`

__Exports the content of a specified area of the current canvas to an image.__

#### Arguments

This method requires an `object` with the following attributes specifying the options:
- `x` (`number`). Horizontal coordinates of the specified canvas area (`0` by default).
- `y` (`number`). Vertical coordinate of the specified canvas area (`0` by default).
- `width` (`number`). Width of the specified canvas area (by default: `width` – `x`).
- `height` (`number`). Height of the specified canvas area (by default: `height` – `y`)..
- `destWidth` (`number`). Width of the generated image (by default: `width` x screen pixel density).
- `destHeight` (`number`). Height of the generated image (by default: `height` x screen pixel density).
- `fileType` (`string`). Format of the generated image, `png` (by default) or `jpg`.
- `quality` (`number`). Image quality, valid for JPG images (values between `0` and `1`.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The function has an `object` argument with the following member:
  - `uri` (`string`). Path of the generated image. 
- `fail` (`function`). Optional callback function corresponding to a failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
this.message = "drawfillStyleforcreatePattern"; 
var test = this.$element("45Canvas"); 
var ctx = test.getContext("2d"); 
console.log("ctx.fillStyle 555 >> " + ctx.fillStyle); 
var img = new Image(); 
img.src = "https://www.example.org/logo.png"; 
img.onload = () => { 
    console.log("Image load success."); 
    var pat = ctx.createPattern(img, "repeat"); 
    ctx.rect(0, 0, 500, 200); 
    ctx.fillStyle = pat; 
    console.log("ctx.fillStyle 666 >> " + ctx.fillStyle); 
    ctx.fill(); 
    test.toTempFilePath({ 
        x: 0, 
        y: 0, 
        width: 500, 
        height: 200, 
        destWidth: 500, 
        destHeight: 200, 
        fileType:"png", 
        quality:1.0, 
        success: (data) => { 
            this.imageSrc = data.uri; 
            console.log(`Canvas toTempFilePath success ${data.uri}`) 
            console.log(`Canvas toTempFilePath success ${data.tempFilePath}`) 
        }, 
        fail: (data) => { 
            console.log('Canvas toTempFilePath data ='+data); 
        }, 
        complete: () => { 
            console.log('Canvas toTempFilePath complete.'); 
        } 
    }) 
} 
img.onerror = function () { 
    console.log("Image load fail."); 
}
```


