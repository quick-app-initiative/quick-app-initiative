# Package Management

__App package management.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.package"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import pkg from '@system.package' 
```

Or

``` js
let pkg = require("@system.package")
```


## Methods

This service has the following methods:

- [`method()`](#method)
- [`hasInstalled({package,success,fail,complete})`](#hasinstalled-package-success-fail-complete)
- [`install({package,success,fail,complete})`](#install-package-success-fail-complete)
- [`getInfo({package,success,fail,complete})`](#getinfo-package-success-fail-complete)
- [`getSignatureDigests({package,success,fail,complete})`](#getsignaturedigests-package-success-fail-complete)


### `hasInstalled({package,success,fail,complete})`

__Method that checks whether a native or quick app is installed in the system.__ 

A quick app is considered as installed only when it has a home screen created.

#### Arguments

This method requires an `object` with the following attributes:
- `package` (`string`). Mandatory attribute with the package name that identifies an app.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
  - `res.result` is a `boolean` that indicates whether the app exists or not.
- `fail` (`function(res, code)`). Optional callback function corresponding to the failed execution.
- `complete` (`function()`). Optional callback function corresponding to the end of the execution.


Example:

``` js 
pkg.hasInstalled({ 
    package: 'org.example.myapp', 
    success: function(data) { 
        console.log("handling success: " + data.result); 
    }, 
    fail: function(data, code) { 
        console.log("handling fail, code=" + code); 
    } 
})
```

### `install({package,success,fail,complete})`

__This method opens the app details page on a app marketplace where the user can install the native app.__ 

This method does not check whether the installation is successful.

#### Arguments

This method requires an `object` with the following attributes:
- `package` (`string`). Mandatory attribute with the package name that identifies an app.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
  - `res.result` is a `boolean` that indicates whether the app exists or not on the marketplace.
- `fail` (`function(res, code)`). Optional callback function corresponding to the failed execution.
- `complete` (`function()`). Optional callback function corresponding to the end of the execution.


Example:

``` js 
pkg.install({ 
    package: 'org.example.myapp', 
    success: function(data) { 
        console.log("handling success: " + data.result); 
    }, 
    fail: function(data, code) { 
        console.log("handling fail, code=" + code); 
    } 
})
```

### `getInfo({package,success,fail,complete})`

__This method gets the version name and version code of an app.__

#### Arguments

This method requires an `object` with the following attributes:
- `package` (`string`). Mandatory attribute with the package name that identifies an app.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
  - `res.versionCode` is a `number` that indicates the version code of the package name indicated in `package`.
  - `res.versionName` is a `string` that indicates the version name of the package name indicated in `package`.
- `fail` (`function(res, code)`). Optional callback function corresponding to the failed execution with the following result codes:
  - `202` : Invalid parameter.
  - `1000` : No apps found.
- `complete` (`function()`). Optional callback function corresponding to the end of the execution.


Example:

``` js 
pkg.getInfo({ 
    package: 'org.example.myapp', 
    success: function(data) { 
        console.log(`handling success: ${data.versionCode}, ${data.versionName}`) 
    }, 
    fail: function(data, code) { 
        console.log("handling fail, code=" + code); 
    } 
})
```

### `getSignatureDigests({package,success,fail,complete})`

__Method to get the app signature digests.__ 

It could be a quick app or a native app.

#### Arguments

This method requires an `object` with the following attributes:
- `package` (`string`). Mandatory attribute with the package name that identifies an app.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
  - `res.signatureDigests` is an `array` with the list of signatures encrypted using the SHA-256 algorithm.
  - `res.versionName` is a `string` that indicates the version name of the package name indicated in `package`.
- `fail` (`function(res, code)`). Optional callback function corresponding to the failed execution with the following result codes:
  - `202` : Invalid parameter.
  - `1000` : No apps found.
- `complete` (`function()`). Optional callback function corresponding to the end of the execution.


Example:

``` js 
pkg.getSignatureDigests({ 
    package: 'org.example.myapp', 
    success: function (data) { 
        data.signatureDigests.map(function (item) { 
            console.log(`handling success: signatureDigests = ${item}`) 
        }) 
    }, 
    fail: function (data, code) { 
        console.log(`handling fail, code = ${code}`) 
    } 
})
```
