# Logging

__Methods for recording messages in the console.__

## Manifest Declaration

This service does not need to be declared in the [manifest's `features`](../guide/manifest.html#features) member.

## Module Import

You don't need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents). You can directly use the `console` variable from your scripts.

## Methods

The `console` variable has the following methods:

- `debug(msg)`
- `log(msg)`
- `info(msg)`
- `warn(msg)`
- `error(msg)`

__Log a message in the console.__

All of them log a message in the console. They have similar effect, but they are intended for specific level of logs (debug, generic log, information, warnings and errors).

#### Arguments

This method have one argument:
- `msg` (`string`). Mandatory argument with the text to be recorded.

#### Return 

This method does not return any value.

Example:

``` js 
[1,2,3,4,5,6].forEach(element => {
    console.log(`Item: ${element}`)
});
```

With the following output:

``` bash
Item: 1
Item: 2
Item: 3
Item: 4
Item: 5
Item: 6
```
