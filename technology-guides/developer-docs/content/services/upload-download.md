# Upload and Download

__Upload and download files.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.request"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import request from '@system.request' 
```

Or

``` js
let request = require("@system.request")
```

## Methods

This service has the following methods:

- [`upload({url,files,header,method,data,success,fail,complete})`](#upload-url-files-header-method-data-success-fail-complete)
- [`download({url,header,description,filename,success,fail,complete})`](#download-url-header-description-filename-success-fail-complete)

### `upload({url,files,header,method,data,success,fail,complete})`

__Method to upload a resource__.

::: tip Timeout configuration 
You can configure the network timeout in the [`config.network` member](../guide/manifest.html#network) of the manifest.
:::

#### Arguments

This method requires an `object` with the following attributes:
- `url` (`string`). Mandatory attribute with the URL of the resource.
- `files` (`array`). Mandatory attribute with the list of files to be uploaded, submitted as `multipart/form-data`. The array include `object`s with the following members:
  - `uri` (`string`). Mandatory attribute with the local path of the files to be uploaded. 
  - `filename` (`string`). Optional attribute with the file name in the header (when submitting `multipart`). If the file name is not specified here, the file name is obtained from the URI by default.
  - `name` (`string`). Optional attribute with the name of the form when `multipart` is submitted. The default value is `file`.
  - `type` (`string`). Optional attribute with the `Content-Type` of the files to be uploaded. If this parameter is not specified, the system will select a `Content-Type` based on the filename or URI extension. The system will show an error if `Content-Type` cannot be obtained, indicating that the parameter is invalid.
- `header` (`object`). Optional attribute with the headers of the request. User agent header is automatic specified.
- `method` (`string`). Optional attribute with the HTTP method: `POST` (default value) and `PUT`. 
- `data` (`array`). Optional attribute with additional form data in an HTTP request. The array contains objects with the following members:
  - `name` (`string`). Mandatory attribute with the name of the form field.
  - `value` (`string`). Mandatory attribute with the value of the form field.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `code` (`number`). Server status code.
  - `data` (`string`). If the value of type in the header returned by the server is `text/*`, `application/json`, `application/javascript`, or `application/xml`, the value is textual. Otherwise, the value is the URI of a temporary file.
  - `headers` (`object`). HTTP headers in the response from the server. 
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The arguments of the callback function are:
  - `msg` (`object`) Message with information about the error (text in `msg.errMsg`).
  - `code` (`number`) Error code.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

::: tip
If you want to use the `application/octet-stream` mode, use the [fetch](./fetch.html#fetch-url-data-header-method-responsetype-success-fail-complete).  
:::

Example:

``` js 
request.upload({ 
    url: "https://www.example.com", 
    header: { "Accept-Encoding": "gzip, deflate", "Accept-Language": "es-ES,en-US;q=0.8,en;q=0.6" }, 
    files: [ 
    { 
        uri: "internal://xxx/xxx/test", 
        name: "file1", 
        type: "test.png" 
    } 
    ], data: [ 
    { 
        name: "key1", 
        value: "value1" 
    } 
    ], 
    success: function (data) {  
        console.log("handling success");  
    }, 
    fail: function (data, code) { 
        console.log("handling fail, code=" + code); 
    } 
})
```

### `download({url,header,description,filename,success,fail,complete})`

__Method to download resources__.

The downloaded file is stored locally on the device, until the user cleans the data of the quick app.

#### Arguments

This method requires an `object` with the following attributes:
- `url` (`string`). Mandatory attribute with the URL of the resource to download.
- `header` (`object`). Optional attribute with the headers of the request. User agent header is automatic specified.
- `description` (`string`). Optional attribute that specifies a description for the download that can be used as the title on the notification bar. The default value is the filename of the resource.
- `filename` (`string`). Optional attribute with the filename of the resource to download. By default is obtained from the network request or the URL. 
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the member:
  - `token` (`string`). Download token, based on which the download status.
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
request.download({ 
    url: "https://www.example.com/file.mp4", 
    success: function (data) {  
        console.log("handling success" + data.token);  
    }, 
    fail: function (data, code) { 
        console.log("handling fail, code=" + code); 
    } 
})
```

## Events

This service provides the following listener:

- [`onDownloadComplete({token,success,fail,complete})`](#ondownloadcomplete-token-success-fail-complete)


### `onDownloadComplete({token,success,fail,complete})`

__Listener for download processes__.

#### Arguments

This method requires an `object` with the following attributes:
- `token` (`string`). Download token.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the member:
  - `uri` (`string`). URI of the file downloaded in the local file system.
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `1000`: Download fails.
  - `1001`: The download task doesn't exist.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
request.onDownloadComplete({ 
    token: "123", 
    success: function (data) { 
        console.log("handling success" + data.uri); 
    }, 
    fail: function (data, code) { 
        console.log("handling fail, code=" + code); 
    } 
})
```

## Example
``` html
<template> 
    <div> 
        <div> 
            <text>{{componentName}}</text> 
        </div> 
        <div> 
            <div> 
                <text>Download file: {{fileDownloadData}}</text> 
            </div> 
            <input type="button" onclick="downloadFile" value="Download file"/> 
            <div> 
                <text>Download file path: {{fileDownloadUri}}</text> 
            </div> 
            <input type="button" onclick="downloadFileComplete" value="File download status"/> 
            <div> 
                <text>Upload file: {{fileUploadData}}</text> 
            </div> 
            <input type="button" onclick="uploadFile" value="Upload file"/> 
        </div> 
    </div> 
</template> 
<style> 
    @import '../Common/css/common.css'; 
    .item-container { 
        margin-bottom: 50px; 
        margin-right: 60px; 
        margin-left: 60px; 
        flex-direction: column; 
    } 
    .item-content { 
        flex-direction: column; 
        background-color: #ffffff; 
        padding-left: 30px; 
        padding-right: 30px; 
        padding-top: 20px; 
        padding-bottom: 20px; 
        margin-bottom: 30px; 
        align-items: flex-start; 
    } 
    .txt { 
        lines: 5; 
        padding-top: 15px; 
        padding-bottom: 15px; 
    } 
</style> 
<script> 
    import request from '@system.request' 
    import prompt from '@system.prompt' 
    export default { 
        data: { 
            componentName: 'request', 
            fileDownloadData: '', 
            fileUploadData: '', 
            fileDownloadUri:"" 
        }, 
        onInit: function () { 
            this.$page.setTitleBar({ text: 'request' }) 
        }, 
        downloadFile: function () { 
            var self = this; 
             // Download a file and save it to the local host. 
          request.download({ 
                url: 'https://www.huawei.com/Assets/CBG/img/logo.png', 
                description: 'This is description.', 
                filename: 'HuaweiLogo.png', 
                success: function (ret) { 
                    self.fileDownloadData = ret.token; 
                    console.log('file_download_data--------' + JSON.stringify(ret.token)); 
                    prompt.showToast({ 
                        message: 'file_download_data--------' + JSON.stringify(ret.token) 
                    }) 
                }, 
              fail: function (errmsg, errcode) { 
                  prompt.showToast({ 
                      message: "Download failure: "+errcode + ':' + errmsg 
                  }) 
              } 
            }) 
        }, 
        downloadFileComplete: function () { 
            var self = this; 
             // Download a file and save it to the local host. 
            console.log('Download a file'); 
            request.onDownloadComplete({ 
                token:self.fileDownloadData, 
                success: function (ret) { 
                    self.fileDownloadUri = ret.uri; 
                    console.log('fileDownloadUri--------' + JSON.stringify(ret.uri)); 
                    prompt.showToast({ 
                        message: 'fileDownloadUri--------' + JSON.stringify(ret.uri) 
                    }) 
                }, 
                fail: function (errmsg, errcode) { 
                    prompt.showToast({ 
                        message: "Download failure: "+errcode + ': ' + errmsg 
                    }) 
                } 
            }) 
        }, 
        uploadFile() { 
            var self = this; 
            request.upload({ 
                url: "https://example.org/uploadDemo/api/uploadFile", 
                header:{"Accept-Encoding": "gzip, deflate","Accept-Language": "es-ES,en-US;q=0.8,en;q=0.6"}, 
                files: [ 
                    { 
                        uri: self.fileDownloadUri, 
                        name: "component_test.png", 
                        type:"multipart/form-data" 
                    } 
                ], 
                success: function (ret) { 
                    self.fileUploadData = ret.data; 
                } 
            }) 
        } 
    } 
</script>
```