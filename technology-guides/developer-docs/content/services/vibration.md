# Vibration

__Vibration management__.

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.vibrator"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import vibrator from '@system.vibrator' 
```

Or

``` js
let vibrator = require("@system.vibrator")
```


## Methods

This service has the following methods:

- [`vibrate({mode})`](#vibrate-mode)

### `vibrate({mode})`

__Triggers vibration up to 1 second.__.

#### Arguments

This method requires an `object` with the following attributes:
- `mode` (`string`). Optional attribute indicating the vibration mode, either `long` (1000 ms) or `short` (35 ms). The default value is `long`.


Example:

``` js 
vibrator.vibrate({ mode: "short" });
```
