# Data Exchange

__Data exchange between quick apps__ 

This service enables quick apps to share data from other quick apps installed in the same device. There are two main data scopes for data exchange:
- _application_: to access data within this scope, you need to specify the package name and signature of the publisher, and to get its authorization.
- _global_: you can access the data within this scope without knowing the specific package name and signature of the publisher. You don't need authorization from the publisher either.

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "service.exchange"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import exchange from '@service.exchange' 
```

Or

``` js
let exchange = require("@service.exchange")
```


## Methods

This service has the following methods:

- [`get({key,package,sign,scope,success,fail,complete})`](#get-key-package-sign-scope-success-fail-complete)
- [`set({key,value,scope,success,fail,complete})`](#set-key-value-scope-success-fail-complete)
- [`remove({key,success,fail,complete})`](#remove-key-success-fail-complete)
- [`clear({success,fail,complete})`](#clear-success-fail-complete)
- [`grantPermission({package,sign,key,success,fail,complete})`](#grantpermission-package-sign-key-success-fail-complete)
- [`revokePermission({package,key,success,fail,complete})`](#revokepermission-package-key-success-fail-complete)

::: warning
_Global_ scoped data supports set and get operations only. 
:::


### `get({key,package,sign,scope,success,fail,complete})`

__Method to read data in the sharing database__.

#### Arguments

This method requires an `object` with the following attributes:
- `key` (`string`). Mandatory attribute	with the key of a key-value pair stored in the database.
- `package` (`string`). Optional attribute with the name of the package, corresponding to the data publisher. This parameter is mandatory when `scope` is `application`. When scope is `global`, this must be empty.
- `sign` (`string`). Optional attribute with the signature of the data publisher. It is encrypted using SHA-256. This parameter is mandatory when `scope` is `application`, and empty when `scope` is `global`.
- `scope` (`string`). Optional attribute with the scope of the data space. The options are `application` (by default) and `global`. 
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The function has an argument that is a `string` with the value corresponding to the `value` in the database. 
- `fail` (`function`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
  - `1000`: No user permission granted.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
exchange.get({ 
    package: 'org.example.myapp', 
    sign: '7a12ec1d66233f20a20141035b1f7937', 
    key: 'token', 
    success: function(ret) { 
        console.log(`handling success, value = ${ret.value}`) 
    }, 
    fail: function(data, code) { 
        console.log(`handling fail, code = ${code}`) 
    } 
})
```

### `set({key,value,scope,success,fail,complete})`

__Method to publish data in the sharing database__.

#### Arguments

This method requires an `object` with the following attributes:
- `key` (`string`). Mandatory attribute	with the key of a key-value pair stored in the database.
- `value` (`string`). Mandatory attribute with the value associated to the `key` in the database.
- `scope` (`string`). Optional attribute with the scope of the data space. The options are `application` (by default) and `global`. 
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
- `fail` (`function`). Optional callback function corresponding to a failed execution. The possible code is:
  - `202`: Invalid parameter.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
exchange.set({ 
    key: 'token', 
    value: '12347979', 
    success: function() { 
        console.log(`handling success`) 
    }, 
    fail: function(data, code) { 
        console.log(`handling fail, code = ${code}`) 
    } 
})
```


### `remove({key,success,fail,complete})`

__Deletes data in the sharing database__.

#### Arguments

This method requires an `object` with the following attributes:
- `key` (`string`). Mandatory attribute	with the key of a key-value pair stored in the database.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
- `fail` (`function`). Optional callback function corresponding to a failed execution.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
exchange.remove({ 
    key: 'token', 
    success: function(ret) { 
        console.log(`handling success, value = ${ret.value}`) 
    }, 
    fail: function(data, code) { 
        console.log(`handling fail, code = ${code}`) 
    } 
})
```


### `clear({success,fail,complete})`

__Deletes all data in the sharing database__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
- `fail` (`function`). Optional callback function corresponding to a failed execution.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
exchange.clear({ 
    success: function() { 
        console.log(`handling success`) 
    }, 
    fail: function(data, code) { 
        console.log(`handling fail, code = ${code}`) 
    } 
})
```

### `grantPermission({package,sign,key,success,fail,complete})`

__Assigns the data read permission to an app.__.

::: tip
By default, apps with the same signature have the permission to read data.
:::

#### Arguments

This method requires an `object` with the following attributes:
- `package` (`string`). Mandatory attribute with the name of the package, corresponding to the data publisher. 
- `sign` (`string`). Mandatory attribute with the signature of the data publisher of an authorized app. It is encrypted using SHA-256.
- `key` (`string`). Optional attribute with the key of a key-value pair stored that receives the permission on. If this parameter is empty, all the keys in the database receive read permission. 
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. 
- `fail` (`function`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
exchange.grantPermission({ 
    sign: '7a12ec1d66233f20a20141035b1f7937', 
    package: 'org.example.myapp', 
    success: function() { 
        console.log(`handling success`) 
    }, 
    fail: function(data, code) { 
        console.log(`handling fail, code = ${code}`) 
    } 
})
```


### `revokePermission({package,key,success,fail,complete})`

__Revoke the data read permission from an app.__.

::: tip
Read permission from apps with the same signature cannot be revoked.
:::

#### Arguments

This method requires an `object` with the following attributes:
- `package` (`string`). Mandatory attribute with the name of the package, corresponding to the data publisher whose permission will be revoked. 
- `key` (`string`). Optional attribute with the key of a key-value pair stored that revokes the permission on. If this parameter is empty, all the keys in the database receive read permission. 
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. 
- `fail` (`function`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
exchange.revokePermission({ 
    package: 'com.example', 
    success: function() { 
        console.log(`handling success`) 
    }, 
    fail: function(data, code) { 
        console.log(`handling fail, code = ${code}`) 
    } 
})
```

