# Download

__File download management.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.downloadtask"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import downloadtask from '@system.downloadtask' 
```

Or

``` js
let downloadtask = require("@system.downloadtask")
```


## Methods

This service has the following methods:

- [`downloadFile({url,header,filename,timeout,success,fail,complete})`](#downloadfile-url-header-filename-timeout-success-fail-complete)
- [`abort()`](#abort)

### `downloadFile({url,header,filename,timeout,success,fail,complete})`

__Creates a download request__.

Every time this method is called, a new `object` is returned.

#### Arguments

This method requires an `object` with the following attributes:
- `url` (`string`). Mandatory attribute with the URL of the resource to download.
- `header` (`object`). Optional attribute with the headers of the request. User agent header is automatic specified.
- `filename` (`string`). Optional attribute with the filename of the resource to download. 
- `timeout` (`number`). Optional attribute that specifies the connection timeout (not the read/write timeout interval) in millisecond  (by default `10000`).
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `statusCode` (`number`). Server status code.
  - `filePath` (`string`). Local file path.
  - `header` (`object`). HTTP headers in the response from the server. 
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The arguments of the callback function are:
  - `msg` (`object`) Message with information about the error (text in `msg.errMsg`).
  - `code` (`number`) Error code.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
this.mytask = downloadtask.downloadFile({ 
    url: 'https://www.example.org/file.jpg',
    success(res) { 
        console.log("Download success. resp = " + JSON.stringify(res)) 
    }, 
    fail(msg, code) { 
        console.log("Download fail. resp = " + JSON.stringify(msg)) 
    } 
})

```

### `abort()`

__Method to cancel a download task.__

Example:

``` js 
this.mytask.abort()
```

## Events

This service provides the following listeners:

- [`onProgressUpdate(function({progress,totalBytesWritten,totalBytesExpectedToWrite}))`](#onprogressupdate-function-progress-totalbyteswritten-totalbytesexpectedtowrite)
- [`offProgressUpdate()`](#offprogressupdate)
- [`onHeadersReceived(function({header}))`](#onheadersreceived-function-header)
- [`offHeadersReceived()`](#offheadersreceived)

### `onProgressUpdate(function({progress,totalBytesWritten,totalBytesExpectedToWrite}))`

__Method to listen to download progress change events__.

Every time this method is called, a new `object` is returned.

#### Arguments

This method requires a callback `function` with an `object` as argument that has the following members:
- `progress` (`number`). Download progress, in percentage.
- `totalBytesWritten` (`number`). Size of the downloaded data, in bytes.
- `totalBytesExpectedToWrite` (`number`). Expected size of data to be downloaded, in bytes.

Example:

``` js 
this.mytask.onProgressUpdate(res => { 
    console.log("Download onProgressUpdate = " + JSON.stringify(res)) 
})
```

### `offProgressUpdate()`

__Method to cancel a listener of download progress change events__.

Example:

``` js 
this.mytask.offProgressUpdate()
```


### `onHeadersReceived(function({header}))`

__Method to listen to HTTP response header events__.

This method needs to be called before the file download is completed.

#### Arguments

This method requires a callback `function` with an `object` as argument that has the following member:
- `header` (`object`). HTTP response headers returned by the server.

Example:

``` js 
this.mytask.onHeadersReceived(res => { 
    console.log("Download onHeaderReceived = " + JSON.stringify(res)) 
})
```

### `offHeadersReceived()`

__Method to cancel a listener of HTTP response headers events__.

Example:

``` js 
this.mytask.offHeadersReceived()
```
