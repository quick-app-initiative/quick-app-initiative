# Screen Brightness

__Control of the screen brightness.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.brightness"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import brightness from '@system.brightness' 
```

Or

``` js
let brightness = require("@system.brightness")
```


## Methods

This service has the following methods:

- [`getValue({success,fail,complete})`](#getvalue-success-fail-complete)
- [`setValue({value,success,fail,complete})`](#setvalue-value-success-fail-complete)
- [`getMode({success,fail,complete})`](#getmode-success-fail-complete)
- [`setMode({mode,success,fail,complete})`](#setmode-mode-success-fail-complete)
- [`setKeepScreenOn({keepScreenOn,success,fail,complete})`](#setkeepscreenon-keepscreenon-success-fail-complete)

### `getValue({success,fail,complete})`

__Method to get the current screen brightness level__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(object)`). Optional callback function for success. The function has an `object` argument with the following attributes:
  - `value` (`number`). Screen brightness. The value is an integer from `0` to `255`.
- `fail` (`function()`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
brightness.getValue({ 
    success: function(ret) {
        console.log("handling success");
    } 
})
```

### `setValue({value,success,fail,complete})`

__Method to set the screen brightness level__.

#### Arguments

This method requires an `object` with the following attributes:
- `value` (`number`). Mandatory attribute with the value of the screen brightness. The value is an integer from `0` to `255`.
- `success`	(`function()`). Optional callback function for success. 
- `fail` (`function()`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.

::: tip Note
- If the `value` parameter is `0` or smaller, the system will apply the value `1`.
- If the `value` parameter is greater than `255`, the system will apply the value `255`.
- If the `value` parameter is a decimal number, the system will take only the integer part.
:::

Example:

``` js 
brightness.setValue({ 
    success:function(data){console.log("handling success"); }, 
    fail: function(data, code){console.log("handling fail, code=" + code);} 
})
```

### `getMode({success,fail,complete})`

__Method to get the current setup mode of the screen brightness__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success. The function has an `object` argument with the following attributes:
  - `mode` (`number`). Indicates the setup mode of the brightness: `0` for manual adjustment and `1` for automatic adjustment.
- `fail` (`function()`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
brightness.getMode({ 
    success:function(ret){console.log("mode is: " + ret.mode);} 
})
```

### `setMode({mode,success,fail,complete})`

__Method to set the screen brightness adjustment mode (manual or automatic)__.

#### Arguments

This method requires an `object` with the following attributes:
- `mode` (`number`). Mandatory attribute with the mode of the screen brightness adjustment. The value is an integer: `0` for manual adjustment, and `1` for an automatic control.
- `success`	(`function()`). Optional callback function for success. 
- `fail` (`function()`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.

::: tip Note
- If the `value` parameter is `0` or smaller, the system will apply the value `1`.
- If the `value` parameter is greater than `255`, the system will apply the value `255`.
- If the `value` parameter is a decimal number, the system will take only the integer part.
:::

Example:

``` js 
brightness.setMode({
    mode: 1, 
    success:function(data){console.log("handling success"); }, 
    fail: function(data, code){console.log("handling fail, code=" + code);} 
})
```

### `setKeepScreenOn({keepScreenOn,success,fail,complete})`

__Method to keep the screen on__.

#### Arguments

This method requires an `object` with the following attributes:
- `keepScreenOn` (`boolean`). Mandatory flag that indicates if the system need to keep the screen steady on (`true`) or not.
- `success`	(`function()`). Optional callback function for success. 
- `fail` (`function()`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
brightness.setKeepScreenOn({ 
    keepScreenOn: true, 
    success: function () { 
        console.log(`handling success`) 
    }, 
    fail: function (data, code) { 
    } 
})
```
